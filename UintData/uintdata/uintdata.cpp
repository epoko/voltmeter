﻿#include "uintdata.h"
#include <limits>

UintData::UintData()
{
    qDebug() << "UintData init!!!";
    DataType = Type_unknown;
}

UintData::~UintData()
{
    qDebug() << "UintData deinit!!!";
}

void UintData::decide_type(char *data, int count)
{
    int i;
    char *pdata = data;
    uint32_t temp;
    if(count < 6)
        return;

    count = count>=104?100:count-4;

    while(count){
        for(i=0; i<DataType_MAX; i++){
            temp = 0;
            memcpy(&temp, pdata, DataTitleLenMap[i]);
            if(temp == DataTitleMap[i]){
                DataType = (enum DataType)i;
                DataTitle = DataTitleMap[i];
                DataTitleLen = DataTitleLenMap[i];
                DataTypeLen = DataTypeLenMap[i];
                qDebug() << "decide data type: " << DataType;
                return;
            }
        }
        pdata++;
        count--;
    }
}

void UintData::deal_data(char *data, int offset, int count)
{
    Frame frame;
    float value;

    // 记录帧 是否合法，开始位置，结束位置，图片尺寸（如果为0，标识其不是图片数据包）
    frame.start_index_ = offset;
    frame.end_index_ = offset+count-1;
    frame.image_size_ = 0;
    frame.is_valid_ = true;

    count -= DataTitleLen;
    if(count%DataTypeLen != 0){
        frame.is_valid_ = false;
        frame_list_.append(frame);
        qDebug() << "err start" << frame.start_index_ << "end" << frame.end_index_;
        return;
    }

    switch(DataType){
    case Type_int8:
        for(; count; count--,data++){
            value = *(int8_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_uint8:
        for(; count; count--,data++){
            value = *(uint8_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_int16:
        for(; count; count-=2,data+=2){
            value = *(int16_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_uint16:
        for(; count; count-=2,data+=2){
            value = *(uint16_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_int32:
        for(; count; count-=4,data+=4){
            value = *(int32_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_uint32:
        for(; count; count-=4,data+=4){
            value = *(uint32_t*)data;
            frame.datas_.append(value);}
    break;
    case Type_float:
        for(; count; count-=4,data+=4){
            value = *(float*)data;
            frame.datas_.append(value);}
    break;
    default:
        return;
    }

    frame_list_.append(frame);
    //qDebug() << "start" << frame.start_index_ << "end" << frame.end_index_;
}

void UintData::deal_nodata(int offset, int count)
{
    Frame frame;
    frame.is_valid_ = true;
    frame.start_index_ = offset;
    frame.end_index_ = offset+count-1;
    frame.image_size_ = 0;
    frame_list_.append(frame);
    qDebug() << "no start" << frame.start_index_ << "end" << frame.end_index_;
}

void UintData::ProcessingDatas(char *data, int count)
{
    frame_list_.clear();

    //if(count % 4 != 0)
    //    qDebug() << "ProcessingDatas" << count;


    if(DataType == Type_unknown){
        decide_type(data, count);
        if(DataType == Type_unknown){
            deal_nodata(0, count);
            return;
        }
    }

    uint32_t temp;
    char *pdata = data;
    char *begin = data;

    while(count>=DataTitleLen){
        memcpy(&temp, pdata, DataTitleLen);
        if(temp == DataTitle){
            pdata += DataTitleLen;
            count -= DataTitleLen;
            deal_data(begin, begin-data, pdata-begin);
            begin = pdata;
            continue;
        }

        //if(pdata-begin > (Channel_MAX*DataTypeLen+DataTitleLen)){
        if(pdata-begin > 68){
            deal_nodata(begin-data, pdata-begin);
            begin = pdata;
        }

        if(begin != data){
            pdata += DataTypeLen;
            count -= DataTypeLen;
        }
        else{
            pdata++;
            count--;
        }
    }
    //if(count)
    //    qDebug() << "count last " << count;

}

