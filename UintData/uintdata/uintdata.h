#ifndef UINTDATA_H
#define UINTDATA_H

#include "dataengineinterface.h"

class UintData : public QObject, public DataEngineInterface
{
    Q_OBJECT
    Q_INTERFACES(DataEngineInterface)
    Q_PLUGIN_METADATA(IID "VOFA+.Plugin.UintData")

public:
    explicit UintData();
    ~UintData();

    void ProcessingDatas(char *data, int count);

private:

#define Channel_MAX 16
    enum DataType {
        Type_unknown = -1,
        Type_int8 = 0,
        Type_uint8,
        Type_int16,
        Type_uint16,
        Type_int32,
        Type_uint32,
        Type_float,
        DataType_MAX
    };

    const uint32_t DataTitleMap[DataType_MAX] = {0xa557, 0xa558, 0xa5a5a5a9, 0xa5a5a5aa, 0xa5a5a5a2, 0xa5a5a5a3, 0x7f8a5a5a};
    const uint8_t DataTitleLenMap[DataType_MAX] = {2, 2, 4, 4, 4, 4, 4};
    const uint8_t DataTypeLenMap[DataType_MAX] = {1, 1, 2, 2, 4, 4, 4};

    enum DataType DataType = Type_unknown;
    uint32_t DataTitle = 0;
    uint8_t DataTitleLen = 0;
    uint8_t DataTypeLen = 0;

    void decide_type(char *data, int count);
    void deal_data(char *data, int offset, int count);
    void deal_nodata(int offset, int count);
};
#endif // UINTDATA_H
