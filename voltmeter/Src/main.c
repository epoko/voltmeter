#include "main.h"

//FOSC = 32MHz

uint32_t timer=0;

void main()
{
    UartInit();
    InitADC();
    seg_init();
    PWM_Init();
    APP_Init();
    
    EA = 1;
    
//    printf("system start!\r\n");
    while(timer<100);
    
    while(1)
    {
        APP_Deal();
    }
}

