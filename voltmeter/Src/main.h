#ifndef __MAIN_H_
#define __MAIN_H_

#include "STC8G.H"
#include "intrins.h"
#include "stdio.h"

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long DWORD;

typedef unsigned char uchar;
typedef unsigned int uint;

typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t;

typedef char int8_t;
typedef int int16_t;
typedef long int32_t;

#define LongToBin(n) \
(\
((n >> 21) & 0x80) | \
((n >> 18) & 0x40) | \
((n >> 15) & 0x20) | \
((n >> 12) & 0x10) | \
((n >> 9) & 0x08) | \
((n >> 6) & 0x04) | \
((n >> 3) & 0x02) | \
((n ) & 0x01) \
)

#define Bin(n) LongToBin(0x##n##l)



extern uint32_t timer;

void UartInit(void);
void usrt_send_buf(uint8_t *buf, uint8_t len);


#define Seg_dot 0x80
void seg_init();
extern uchar code Seg_map[10];
extern uint8_t seg_buf[4];


#define ADC_TIME_MAX 2000
void InitADC();
extern bit ADC_over;
extern uint32_t ADC_Value[2];
extern uint16_t ADC_ref;

void PWM_Init();
void pwm_set(uint16_t value);

void APP_Init();
void APP_Deal();

#endif
