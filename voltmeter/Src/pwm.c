#include "main.h"

void PWM_Init()
{
    P3M0 |= Bin(10000000);
    P3M1 &= ~Bin(10000000);
    
    CCON = 0;                       //初始化PCA控制寄存器
                                    //PCA定时器停止
                                    //清除CF标志
                                    //清除模块中断标志
    CL = 0;                         //复位PCA寄存器
    CH = 0;
    CMOD = 0x08;                    //设置PCA时钟源为系统时钟
                                    //禁止PCA定时器溢出中断
	
    CCAPM2 = 0x42;                  //PCA模块2为PWM工作模式
    PCA_PWM2 = 0xff;                //PCA模块2输出8位PWM
    CCAP2L = 0xff;                  //PWM占空比为87.5%[(100H-20H)/100H]
    CCAP2H = 0xff;
	
    CR = 1;                         //PCA定时器开始工作
}

void pwm_set(uint16_t value)
{
    if(value)
    {
        if(value<1024)
            value = 1024-value;
        else
            value = 0;
        PCA_PWM2 = (PCA_PWM2&0xCD) | ((value>>4)&0x30);
        CCAP2H = value;
    }
    else
    {
        PCA_PWM2 = (PCA_PWM2&0xCD) | 0x02;
    }
}
