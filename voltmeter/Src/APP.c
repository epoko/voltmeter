#include "main.h"

#define APP_MODE_MAX 4
#define KEY P32

#define seg_display(a,b,c,d) \
seg_buf[0] = a; \
seg_buf[1] = b; \
seg_buf[2] = c; \
seg_buf[3] = d; \

uint16_t Vk,Ik;
uint16_t ADC_V=0;   //1mV
uint32_t ADC_I=0;   //0.1mA
uint16_t ADC_REF_rc=972;
uint8_t display_mode=0;

void display_V()
{
	if(ADC_V>=10000)
	{
		seg_display(Seg_map[ADC_V/10000],
                     Seg_map[ADC_V/1000%10]|Seg_dot,
                     Seg_map[ADC_V/100%10],
                     Seg_map[ADC_V/10%10]);

	}
	else
	{
		seg_display(Seg_map[ADC_V/1000]|Seg_dot,
                     Seg_map[ADC_V/100%10],
                     Seg_map[ADC_V/10%10],
                     Seg_map[ADC_V%10]);
	}
}
	

void display_I()
{
	if(ADC_I<10000)
	{
		seg_display(Seg_map[ADC_I/1000],
                     Seg_map[ADC_I/100%10],
                     Seg_map[ADC_I/10%10]|Seg_dot,
                     Seg_map[ADC_I%10]);
	}
	else if(ADC_I<100000)
	{
		seg_display(Seg_map[ADC_I/10000]|Seg_dot,
                     Seg_map[ADC_I/1000%10],
                     Seg_map[ADC_I/100%10],
                     Seg_map[ADC_I/10%10]);
	}
    else
	{
		seg_display(Seg_map[ADC_I/100000],
                     Seg_map[ADC_I/10000%10]|Seg_dot,
                     Seg_map[ADC_I/1000%10],
                     Seg_map[ADC_I/100%10]);
	}
}

void display_B()
{
	if((timer>>12)&1)
		display_V();
	else
		display_I();
}

void display_P()
{
	uint32_t power;
	power = (uint32_t)ADC_V*ADC_I/10000;
	
	if(power<10000)
	{
		seg_display(Seg_map[power/1000]|Seg_dot,
                     Seg_map[power/100%10],
                     Seg_map[power/10%10],
                     Seg_map[power%10]);
	}
	else if(power<100000)
	{
		seg_display(Seg_map[power/10000],
                     Seg_map[power/1000%10]|Seg_dot,
                     Seg_map[power/100%10],
                     Seg_map[power/10%10]);
	
	}
	else
	{
		seg_display(Seg_map[power/100000],
                     Seg_map[power/10000%10],
                     Seg_map[power/1000%10]|Seg_dot,
                     Seg_map[power/100%10]);
	
	}
}


void display_M(char mode)
{
	uint8_t str;
	switch(mode)
	{
		case 0:str=0x3e;break;		//V
		case 1:str=0x77;break;		//I
		case 2:str=0x39;break;		//C
		case 3:str=0x73;break;		//P
	}
	
	seg_display(Seg_map[mode], 0x40, 0, str);
}

void APP_Init()
{
    uint16_t BGV;
    
    P3M0 &= ~Bin(00000100);
    P3M1 &= ~Bin(00000100);
    
    BGV = *(uint16_t idata *)0xef;
    Vk = (uint32_t)BGV*61/10;
    Ik = BGV*20;
    
    display_M(display_mode);
}

void APP_Deal()
{
	static uint32_t time_last=500;
    
	static uint32_t key_time=0;
    static uint8_t key_state=0;
    static bit key_low=0;
    
    if(timer != key_time)
    {
        key_time = timer;
        if(KEY)
        {
            if(key_state<10)
                key_state++;
            else if(key_low)
            {
                key_low=0;
                
                display_mode++;
                if(display_mode >= APP_MODE_MAX)
                    display_mode=0;
                
                time_last = timer + 500;
                display_M(display_mode);
            }
        }
        else
        {
            if(key_state)
                key_state--;
            else
                key_low=1;
        }
    }
	
    if(ADC_over && timer >= time_last)
    {
        ADC_over = 0;
        
        ADC_REF_rc = (ADC_ref<<1) + (ADC_REF_rc>>1);
        //vcc = 1190/ref*4096
        
        //ADC_V = ADC_Value[0]/ADC_TIME_MAX * 6.1*1190/ref*4096 /4096;    //6.1*vcc
        ADC_V = ADC_Value[0]/ADC_REF_rc *Vk /ADC_TIME_MAX;    //6.1*vcc
        
        //ADC_I = ADC_Value[1]/ADC_TIME_MAX * 20*1190/ref*4096 /4096;   //20*vcc
        ADC_I = ADC_Value[1]/ADC_REF_rc *Ik /ADC_TIME_MAX;   //max:
        
        pwm_set(ADC_I/10);
        
		switch(display_mode)
		{
			case 0:display_V();break;
			case 1:display_I();break;
			case 2:display_B();break;
			case 3:display_P();break;
		}
    }
		
	//	printf("V=%u\tI=%u\tT=%d\n", ADC_V, ADC_I, ADC_T);
}
