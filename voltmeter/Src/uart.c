#include "main.h"

#define FOSC            32000000UL
#define BRT             (65536 - FOSC / 2000000 / 4)

static char *send_buf;
static char send_len = 0;
bit busy = 0;

void UartInit(void)		//2000000bps@32.000MHz
{
	SCON = 0x40;		//8位数据,可变波特率
	AUXR |= 0x01;		//串口1选择定时器2为波特率发生器
	AUXR |= 0x04;		//定时器时钟1T模式
	T2L = BRT;		//设置定时初始值
	T2H = BRT>>8;		//设置定时初始值
	AUXR |= 0x10;		//定时器2开始计时
    
	ES = 1;
}

void UART1_Isr() interrupt 4
{
//	if(RI)
//	{
//		RI = 0;                                 //清中断标志
//	}
	if(TI)
	{
		TI = 0;                                 //清中断标志
        if(send_len)
        {
            SBUF = *send_buf++;
            send_len--;
        }
        else
            busy = 0;
	}
}

#if 0
char putchar(char c)
{
	while(busy);
    SBUF = c;
    busy = 1;
	return c;
}
#endif

void usrt_send_buf(uint8_t *buf, uint8_t len)
{
    send_buf = buf+1;
    send_len = len-1;
    SBUF = *buf;
    busy = 1;
}
